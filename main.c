/*
  tscsmoneysave
  
  S.T.A.L.K.E.R.: Clear Sky money editor.
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "minilzo.h"

#define P_ERROR(msg, ...) fprintf(stderr, "ERROR: "msg, ##__VA_ARGS__)
#define CLEANUP(function) __attribute__ ((__cleanup__(function)))
#define VERSION_STRING "1.0.0"
#define SCS_MAGIC_SAVE 0xFFFFFFFF

bool g_verbose;

#define VERBOSE(msg, ...) {if (g_verbose) { printf(msg, ##__VA_ARGS__);}}

typedef struct 
{
	uint32_t magic;
	uint32_t version;
	uint32_t size;
} Header;

typedef struct 
{
	size_t size;
	Header header;
	uint8_t compressedData[0];
} SaveFile;

typedef struct 
{
	size_t  size;
	uint32_t objectsChunkIndex;
	uint32_t objectsChunkSize;
	uint32_t playerObjectIndex;
	uint32_t playerObjectSize;
	uint32_t playerUpdateIndex;
	uint32_t playerUpdateSize;
	uint32_t *moneyInventory;
	uint32_t *moneyState;
	uint8_t *buffer;
} Data;

typedef struct
{
	uint8_t *start;
	uint8_t *end;
	uint8_t *current;
} DataStream;

bool DataStream_advance (DataStream *ds, size_t step)
{
	assert(ds);
	ds->current += step;
	return (ds->current <= ds->end);
}

uint8_t DataStream_u8 (DataStream *ds)
{
	assert(ds);
	uint8_t u8 = *(uint8_t*)ds->current;
	ds->current += 1;
	return u8;
}

uint16_t DataStream_u16 (DataStream *ds)
{
	assert(ds);
	uint16_t u16 = *(uint16_t*)ds->current;
	ds->current += 2;
	return u16;
}

uint32_t DataStream_u32 (DataStream *ds)
{
	assert(ds);
	uint32_t u32 = *(uint32_t*)ds->current;
	ds->current += 4;
	return u32;
}

float DataStream_r32 (DataStream *ds)
{
	assert(ds);
	float r32 = *(float*)ds->current;
	ds->current += 4;
	return r32;
}

void DataStream_rewind (DataStream *ds)
{
	assert(ds);
	ds->current = ds->start;
}

bool DataStream_string (DataStream *ds, uint8_t *buffer, size_t bSize)
{
	assert(ds);
	for (size_t i = 0; i < bSize; ++i)
	{
		uint8_t c = *(ds->current++);
		buffer[i] = c;
		if ('\0' == c)
			return true;
	}
	return false;
}

bool DataStream_bypassString (DataStream *ds)
{
	assert(ds);
	while(*(ds->current++) != '\0')
	{
		if (ds->current > ds->end)
			return false;
	}
	return true;
}

void printHeader (void)
{
	puts("tscsmoneysave v"VERSION_STRING"\n");
}

void printHelp (void)
{
	puts("Usage:\n"
	     "  tscssedit <options> <path to save file>\n\n"
	     "Options:\n"
	     "  -n, --verbose: Enable verbose mode.");
}

bool isHeaderValid (Header *header)
{
	assert(header);
	return (header->magic == SCS_MAGIC_SAVE) &&
	       (header->version == 5) &&
		   (header->size >= 8);
}

void cleanupFile(FILE **fp)
{
  if (*fp)
	fclose(*fp);
}

bool checkMalloc(void *p)
{
	if (!p)
	{
		P_ERROR("Memory allocation failed.");
		return false;
	}
	return true;
}

SaveFile *loadFile (char *path)
{
	assert(path);	
	FILE *save CLEANUP(cleanupFile) = fopen(path, "rb");
	if (!save)
	{
		P_ERROR("Couldn't open save file: ");
		perror("");
		return NULL;
	}	
	fseek(save, 0, SEEK_END);
	long fsize = ftell(save);
	fseek(save, 0, SEEK_SET);
	SaveFile *sf = malloc(fsize + sizeof(size_t) + 1);
	if (!checkMalloc(sf))
		return NULL;
	sf->size = fsize;
	size_t read = fread(&sf->header, fsize, 1, save);
	if (1 != read)
	{
			P_ERROR("Couldn't read save file.");
			return NULL;
	}
	return sf;
}

bool saveFile (SaveFile *sf, char *path)
{
	assert(sf);
	FILE *save CLEANUP(cleanupFile) = fopen(path, "wb");
	if (!save)
	{
		P_ERROR("Couldn't save edited file: ");
		perror("");
		return false;
	}
	fwrite(&sf->header, sizeof(Header), 1, save);
	fwrite(&sf->compressedData, sf->size, 1, save);
	return true;	
}

Data *unpack (SaveFile *fs)
{
	assert(fs);	
	uint8_t *buffer = malloc(fs->header.size);
	lzo_uint ds;	
	if (!checkMalloc(buffer))
	{
		return NULL;
	}
	int r = lzo1x_decompress((const lzo_byte*)&fs->compressedData, 
	                         fs->size - (sizeof(uint32_t) * 3),
				 (lzo_byte*)buffer,
				 &ds,
				 NULL);
	if (LZO_E_OK != r || ds != fs->header.size)
	{
		free(buffer);
		return NULL;
	}
	Data *data = malloc(sizeof(Data));
	if (!checkMalloc(data))
	{
		free(buffer);
		return NULL;
	}
	data->size = ds;
	data->buffer = buffer;
	data->objectsChunkIndex = 0;
	data->objectsChunkSize  = 0;
	data->playerObjectIndex = 0;
	data->playerObjectSize  = 0;
	data->moneyInventory = NULL;
	data->moneyState = NULL;
	return data;
}

SaveFile *pack (Data *d)
{
	assert(d);
	SaveFile *sf = malloc(sizeof(SaveFile) + d->size);
	if (!checkMalloc(sf))
		return NULL;
	sf->header.magic   = SCS_MAGIC_SAVE;
	sf->header.version = 5;
	sf->header.size    = d->size;
	lzo_voidp workspace = malloc(LZO1X_1_MEM_COMPRESS);
	if (!checkMalloc(workspace))
	{
		free(sf);
		return NULL;
	}
	lzo1x_1_compress((const lzo_byte*)d->buffer, 
				     d->size, 
				     (lzo_byte*)&sf->compressedData,
					 &sf->size,
					 workspace);
	return sf;
}

bool isChunkCompressed (uint32_t type)
{
	return type & 0x80000000;
}

void scanChunks (Data *d)
{
	assert(d);
	d->objectsChunkIndex = 0;
	uint32_t index = 0;
	VERBOSE("   TYPE |      SIZE |           OFFSET | COMPRESSED?\n"
	        "--------|-----------|------------------|------------\n");
	while (index < d->size)
	{
		uint32_t *du32 = (uint32_t*)&d->buffer[index];
	    int32_t type = du32[0] & 0x7fffffff;
		uint32_t csize = du32[1];
		index += 2 * sizeof(uint32_t);
		VERBOSE("%7u | %9u | %10p | %11s\n",
		        type & 0x7fffffff,
		        csize,
		        index,
			    isChunkCompressed(du32[0]) ? "Yes" : "No");
		if (2 == type)
		{
			d->objectsChunkIndex = index;
			d->objectsChunkSize = csize;
		}
		index += csize;	
	}
}

/*
 * Finds the single_player object in the objects chunk.
 */
bool analyzeObjectsChunk(Data *d)
{
	assert(d);
	assert(0 != d->objectsChunkIndex);
	assert(0 != d->objectsChunkSize);
	DataStream ds = {
		.start = &d->buffer[d->objectsChunkIndex],
		.current = &d->buffer[d->objectsChunkIndex],
		.end = &d->buffer[d->objectsChunkIndex] + d->objectsChunkSize
	};
	uint32_t objectsCount = DataStream_u32(&ds);
	VERBOSE("%u objects found.\n", objectsCount);
	for (uint32_t i = 0; i < objectsCount; ++i)
	{
		uint16_t size = DataStream_u16(&ds);
		DataStream oDs = {
			.start = ds.current,
			.current = ds.current,
			.end = ds.current + size
		};
		ds.current += size;
		uint16_t type = DataStream_u16(&oDs);
		uint8_t name[64];
		DataStream_string(&oDs, name, 64);
		uint8_t name2[1024];
		DataStream_string(&oDs, name2, 1024);
		//update packet size
		uint16_t uSize = DataStream_u16(&ds);
		if (!strcmp(name2, "single_player"))
		{
			d->playerObjectIndex = oDs.start - d->buffer;
			d->playerObjectSize = size;		
			d->playerUpdateSize = uSize;
			d->playerUpdateIndex = ds.current - d->buffer;
			return true;
		}	
		ds.current += uSize; //bypass update packet
		/*
		if (100 > i)
			printf("%u = %u %u %s %s\n", i, size, type, name, name2);
		*/
	}
	return false;
}

bool analyzePlayerObject (Data *d)
{
	assert(d);
	assert(0 != d->playerObjectIndex);
	assert(0 != d->playerObjectSize);
	DataStream ds = {
		.start = &d->buffer[d->playerObjectIndex],
		.current = &d->buffer[d->playerObjectIndex],
		.end = &d->buffer[d->objectsChunkIndex] + d->playerObjectSize
	};
	if (1 != DataStream_u16(&ds))
	{
		P_ERROR("Invalid player object.");
		return false;
	}
	DataStream_bypassString(&ds);
	DataStream_bypassString(&ds);
	DataStream_advance(&ds, 34);
	uint16_t flags = DataStream_u16(&ds);
	if (flags & (1<<5))
		DataStream_advance(&ds, 2);
	DataStream_advance(&ds, 4);
	uint16_t clientDataSize = DataStream_u16(&ds);
	DataStream stateDs = {
		.start = ds.current + clientDataSize + 4,
		.current = ds.current + clientDataSize + 4,
		.end = ds.end,
	};
	DataStream_advance(&ds, 1);
	if (DataStream_u8(&ds))
	{
		DataStream_advance(&ds, 16);
		uint8_t numWounds = DataStream_u8(&ds);
		DataStream_advance(&ds, numWounds * 12);
	}
	DataStream_advance(&ds, 11);
	DataStream_bypassString(&ds);
	DataStream_bypassString(&ds);
	d->moneyInventory = (uint32_t*)ds.current;
	DataStream_advance(&stateDs, 18);
	DataStream_bypassString(&stateDs);
	DataStream_advance(&stateDs, 8);
	DataStream_bypassString(&stateDs);
	DataStream_advance(&stateDs, 8);
	DataStream_advance(&stateDs, 2 * DataStream_u32(&stateDs));
	DataStream_advance(&stateDs, 2 * DataStream_u32(&stateDs));
	DataStream_advance(&stateDs, 10);
	d->moneyState = (uint32_t*)stateDs.current;
	return true;
}

int main (int argc, char **argv)
{
	printHeader();
	char *path = argv[1];
	if (1 >= argc)
	{
		P_ERROR("Invalid or missing path to save file.\n\n");
		printHelp();
		return 0;
	}
	else if ( 2 <= argc)
	{
		if (!strcmp(argv[1], "-v") || !strcmp(argv[1], "--verbose"))
			g_verbose = true;
		path = argv[2];
	}
	VERBOSE("Loading %s... ", path);
	SaveFile *sf = loadFile(path);
	if (sf)
	{
		puts("OK!");
	}
	else
	{
		return 0;
	}
	VERBOSE("Verifying header... ");
	if (isHeaderValid(&sf->header))
	{
		puts("OK!");
	}
	else
	{
		free(sf);
		P_ERROR("Bad header.\n");
		return 0;
	}
	VERBOSE("Unpacking save file... ");
	Data *data = unpack(sf);
	free(sf);
	sf = NULL;
	if (data)
	{
		puts("OK!");
	}
	else
	{
		P_ERROR("Unpacking failed.");
		return 1;
	}
	
	// save unpacked save
	/*
	FILE *t = fopen("unpacked.bin", "wb");
	fwrite(data->buffer, data->size, 1, t);
	fclose(t);
	*/

	VERBOSE("\n\nScanning for chunks ...\n\n");
	scanChunks(data);	
	VERBOSE("\nAnalyzing objects chunk...");
	if (analyzeObjectsChunk(data))
	{
		VERBOSE("single_player object found at offset %p (size: %u)\n"
		        "              update found at offset %p (size: %u)\n",
			    data->playerObjectIndex,
			    data->playerObjectSize,
			    data->playerUpdateIndex,
			    data->playerUpdateSize);
	}
	else
	{
		P_ERROR("Couldn't find the single_player object.");
		free(data);
		return 0;
	}
	VERBOSE("\nAnalyzing player object...");
	analyzePlayerObject(data);
	if (*data->moneyInventory != *data->moneyState)
	{
		free(data);
		P_ERROR("The two money values aren't equal.");
		return 0;
	}
	printf("Current amount of money: %u %u\n",
	       *data->moneyInventory,
	       *data->moneyState);
	printf("Enter new amount of money (4294967295 at most): ");
	scanf("%u", data->moneyInventory);
	*data->moneyState = *data->moneyInventory;
	printf("New amount of money: %u\n", *data->moneyInventory);
	VERBOSE("Packing the edited file...");
	sf = pack(data);
	if (!sf)
	{
		P_ERROR("Couldn't pack the edited file.");
		return 1;
	}
	free(data);
	char epath[512];
	strcpy(epath, path);
	strcpy(epath + strlen(path) - 4, ".money.sav");
	printf("Saving as %s...", epath);
	saveFile(sf, epath);
	free(sf);
	puts("Done!");
}

