tscsmoneysave
=============

A tool to edit the amount of money of a save file for S.T.A.L.K.E.R.: Clear Sky v1.5.10

How to build
------------

With `gcc *.c` in a terminal.  
Require GCC or MinGW.

This code rely on MiniLZO  
http://www.oberhumer.com/opensource/lzo/  
Copyright (C) 1996-2017 Markus Franz Xaver Johannes Oberhumer

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)